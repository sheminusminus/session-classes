//
//  SessionData.swift
//  TimeTrackerSwift
//
//  Created by Emily Kolar on 1/25/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import Foundation

enum Section: Int {
	case project = 0, module = 1, issue, category, weekEnding
}

class SessionData {
	
	static let sessionData = SessionData()
	
	private let _K_REMEMBER_ME = "kRememberMe"
	private let _K_PROJECT_INFO = "ProjectInfoConfig"
	private var _userID: Int!
	private var _sessionToken: String!
	
	var selectedConfig: [String: Any]!
	var weekEndings: [String]!
	var projectInfo: [[Project]]!
	var timeEntriesInfo: [[String: Any]]!
	var selectedWeekID: Int!
	
	private init() {
		self._userID = 0
		self._sessionToken = ""
		self.selectedConfig = [String: Any]()
		self.weekEndings = [String]()
		self.projectInfo = [[Project]]()
		self.timeEntriesInfo = [[String: Any]]()
		self.selectedWeekID = 0
	}
	
	var uid: Int {
		get {
			return self._userID!
		}
	}
	
	var token: String {
		get {
			return self._sessionToken!
		}
	}
	
	func clear() {
		self._userID = 0
		self._sessionToken = ""
		self.selectedConfig = [String: Any]()
		self.weekEndings = [String]()
		self.projectInfo = [[Project]]()
		self.timeEntriesInfo = [[String: Any]]()
		self.selectedWeekID = 0
	}
	
	func setUserId(userIdVal: Int?) {
		if userIdVal != nil {
			self._userID = userIdVal!
		}
		else {
			self._userID = 0
		}
	}
	
	func setToken(tokenVal: String?) {
		if tokenVal != nil {
			self._sessionToken = tokenVal!
		}
		else {
			self._sessionToken = ""
		}
	}
	
	func keyForDataType(dataType: Section) -> String {
		switch (dataType) {
			case .project:
				return _K_PROJECT_INFO
			default:
				return ""
		}
	}
	
	func nameForDataType(dataType: Section) -> String {
		switch (dataType) {
			case .module:
				return "ModuleName"
			default:
				return "Name"
		}
	}
	
	func displayNameForDataType(dataType: Section) -> String {
		switch (dataType) {
			case .project:
				return "Project"
			case .module:
				return "Module"
			case .issue:
				return "Issue"
			case .category:
				return "Category"
			default:
				return ""
			}
	}
	
	func rememberMeIsOn() -> Bool {
		return UserDefaults.standard.bool(forKey: _K_REMEMBER_ME)
	}
	
	func setRememberMeOn(on: Bool) {
		UserDefaults.standard.set(on, forKey: _K_REMEMBER_ME)
		UserDefaults.standard.synchronize()
	}
	
	func getStoredUsername() -> String? {
		let wrapper = KeychainWrapper()
		return wrapper.myObject(forKey: kSecAttrAccount) as? String
	}
	
	func getStoredPassword() -> String? {
		let wrapper = KeychainWrapper()
		return wrapper.myObject(forKey: kSecValueData) as? String
	}
	
	func setStoredPassword(password: String?, forUsername: String?) {
		let wrapper = KeychainWrapper()
		if let pass = password {
			if let name = forUsername {
				wrapper.mySetObject(name, forKey: kSecAttrAccount)
				wrapper.mySetObject(pass, forKey: kSecValueData)
				wrapper.writeToKeychain()
				return
			}
		}
		wrapper.mySetObject("", forKey: kSecAttrAccount)
		wrapper.mySetObject("", forKey: kSecValueData)
		wrapper.writeToKeychain()
	}
	
	func sumOfHoursForWeekEntries() -> Float {
		var sum: Float = 0
		for entry in timeEntriesInfo {
			sum += entry["Duration"] as! Float
		}
		return sum
	}
}
