//
//  Connection.swift
//  TimeTrackerSwift
//
//  Created by Emily Kolar on 2/1/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import Foundation
import UIKit

class Connection {
	
	static let sharedConnection = Connection()
	
	private var _session: URLSession!
	private var _internetConnection: Bool!
	
	private let _BASE_URL = "http://50.63.53.33:8083"
	private let _LOGIN_URL_STRING = "AuthenticationService.svc/SignIn"
	private let _WEEK_ENDINGS_URL_STRING = "TimeTrackerService.svc/weekEndings"
	private let _PROJECT_INFO_URL_STRING = "/TimeTrackerService.svc/employees"
	private let _TIME_ENTRIES_URL_STRING = "/TimeTrackerService.svc/employees"
	private let _ADD_TIME_URL_STRING = "/TimeTrackerService.svc/timeEntries/add"
	private let _DELETE_ENTRY_URL_STRING = "/TimeTrackerService.svc/timeEntries/delete"
	
	private init() {
		self._internetConnection = testConnection()
		self._session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: OperationQueue.main)
		self._session.configuration.httpMaximumConnectionsPerHost = 1
		self._session.configuration.timeoutIntervalForRequest = 20
		self._session.configuration.timeoutIntervalForResource = 60
		self._session.configuration.httpAdditionalHeaders = ["Accept": "application/json", "Content-Type": "application/json"]
	}
	
	var session: URLSession {
		get {
			return self._session
		}
	}
	
	var internetConnection: Bool {
		get {
			return self._internetConnection
		}
	}
	
	func testConnection() -> Bool {
		let hostReachability = Reachability(hostName: "google.com")
		let internetReachability = Reachability.forInternetConnection()
		let hostStatus = hostReachability?.currentReachabilityStatus()
		let netStatus = internetReachability?.currentReachabilityStatus()
		switch (hostStatus!) {
			case ReachableViaWWAN:
				return true
			case ReachableViaWiFi:
				return true
			default:
				break
		}
		switch (netStatus!) {
			case ReachableViaWWAN:
				return true
			case ReachableViaWiFi:
				return true
			default:
				break
		}
		return false
	}
	
	func login(withUsername: String, password: String, completionBlock: @escaping (Bool) -> Void) {
		var request = URLRequest(url: URL(string: _LOGIN_URL_STRING, relativeTo: URL(string: _BASE_URL))!)
		request.httpMethod = "POST"
		request.setValue("application/json", forHTTPHeaderField: "content-type")
		let base64Pass = password.data(using: .utf8)!.base64EncodedString()
		let dataDict = ["username": withUsername, "password": base64Pass] as [String : Any]
		do {
			let postData = try JSONSerialization.data(withJSONObject: dataDict, options: JSONSerialization.WritingOptions.prettyPrinted)
			UIApplication.shared.isNetworkActivityIndicatorVisible = true
			let task = _session.uploadTask(with: request, from: postData, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
				do {
					let dict = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: [String: Any]]
					
					let resp = response as! HTTPURLResponse
					SessionData.sessionData.setToken(tokenVal: (dict["d"]?["Token"] as! String?))
					SessionData.sessionData.setUserId(userIdVal: dict["d"]?["UserId"] as! Int?)
					completionBlock(resp.statusCode < 400 && SessionData.sessionData.token.characters.count > 0)
					UIApplication.shared.isNetworkActivityIndicatorVisible = false
				}
				catch let err {
					print(err)
				}
			
			})
			task.resume()
		}
		catch let postError {
			print(postError)
		}
		
	}
	
	func isLoggedIn() -> Bool {
		return SessionData.sessionData.token.characters.count > 0
	}
	
	func logOut() -> Void {
		_session.invalidateAndCancel()
	}
	
	func fetchWeekEndingsCompletionBlock(completionBlock: @escaping ([Any]?) -> Void) {
		var request = URLRequest(url: URL(string: _WEEK_ENDINGS_URL_STRING, relativeTo: URL(string: _BASE_URL))!)
		request.httpMethod = "GET"
		request.setValue("application/json", forHTTPHeaderField: "content-type")
		UIApplication.shared.isNetworkActivityIndicatorVisible = true
		let task = _session.dataTask(with: request, completionHandler: {(data: Data?, response: URLResponse?, error: Error?) in
			do {
				let responseArray = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
				let resp = response as! HTTPURLResponse
				completionBlock(resp.statusCode < 400 ? [responseArray] : nil)
				UIApplication.shared.isNetworkActivityIndicatorVisible = false
			}
			catch let err {
				print(err)
			}
		})
		task.resume()
	}

	func fetchProjectInfoCompletionBlock(completionBlock: @escaping ([Any]?) -> Void) {
		let endpointString = "\(_PROJECT_INFO_URL_STRING)/\(SessionData.sessionData.uid)/projects"
		var request = URLRequest(url: URL(string: endpointString, relativeTo: URL(string: _BASE_URL))!)
		request.httpMethod = "POST"
		request.setValue("application/json", forHTTPHeaderField: "content-type")
		let dataDict = ["token": SessionData.sessionData.token]
		do {
			let postData = try JSONSerialization.data(withJSONObject: dataDict, options: JSONSerialization.WritingOptions.prettyPrinted)
			UIApplication.shared.isNetworkActivityIndicatorVisible = true
			let task = _session.uploadTask(with: request, from: postData, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
				do {
					let responseArray = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
					let resp = response as! HTTPURLResponse
					completionBlock(resp.statusCode < 400 ? [responseArray] : nil)
					UIApplication.shared.isNetworkActivityIndicatorVisible = false
				}
				catch let respError {
					print(respError)
				}
			})
			task.resume()
		}
		catch let postError {
			print(postError)
		}
	}
	
	func fetchTimeEntries(forWeek: Int, completionBlock: @escaping ([Any]?) -> Void) {
		let endpointString = "\(_TIME_ENTRIES_URL_STRING)/\(SessionData.sessionData.uid)/timeEntries/\(forWeek)"
		var request = URLRequest(url: URL(string: endpointString, relativeTo: URL(string: _BASE_URL))!)
		request.httpMethod = "POST"
		request.setValue("application/json", forHTTPHeaderField: "content-type")
		let dataDict = ["token": SessionData.sessionData.token]
		do {
			
			let postData = try JSONSerialization.data(withJSONObject: dataDict, options: JSONSerialization.WritingOptions.prettyPrinted)
			UIApplication.shared.isNetworkActivityIndicatorVisible = true
			let task = _session.uploadTask(with: request, from: postData, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
				do {
					let responseArray = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
					let resp = response as! HTTPURLResponse
					completionBlock(resp.statusCode < 400 ? [responseArray] : nil)
					UIApplication.shared.isNetworkActivityIndicatorVisible = false
				}
				catch let respError {
					print(respError)
				}
			})
			task.resume()
		}
		catch let postError {
			print(postError)
		}
	}
	
	func addTime(time: [String: Any], completionBlock: @escaping (Bool) -> Void) {
		var request = URLRequest(url: URL(string: _ADD_TIME_URL_STRING, relativeTo: URL(string: _BASE_URL))!)
		request.httpMethod = "POST"
		request.setValue("application/json", forHTTPHeaderField: "content-type")
		let dataDict = ["token": SessionData.sessionData.token, "entryLog": time] as [String : Any]
		do {
			
			let postData = try JSONSerialization.data(withJSONObject: dataDict, options: JSONSerialization.WritingOptions.prettyPrinted)
			UIApplication.shared.isNetworkActivityIndicatorVisible = true
			let task = _session.uploadTask(with: request, from: postData, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
				completionBlock(!(error != nil))
				UIApplication.shared.isNetworkActivityIndicatorVisible = false
			})
			task.resume()
		}
		catch let postError {
			print(postError)
		}
	}
	
	func deleteEntryWithId(entryId: Int, completionBlock: @escaping ([Any]?) -> Void) {
		let endpointString = "\(_DELETE_ENTRY_URL_STRING)/\(entryId)"
		var request = URLRequest(url: URL(string: endpointString, relativeTo: URL(fileURLWithPath: _BASE_URL))!)
		request.httpMethod = "POST"
		request.setValue("application/json", forHTTPHeaderField: "content-type")
		let dataDict = ["token": SessionData.sessionData.token]
		do {
			
			let postData = try JSONSerialization.data(withJSONObject: dataDict, options: JSONSerialization.WritingOptions.prettyPrinted)
			UIApplication.shared.isNetworkActivityIndicatorVisible = true
			let task = _session.uploadTask(with: request, from: postData, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
				do {
					let responseArray = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
					let resp = response as! HTTPURLResponse
					completionBlock(resp.statusCode < 400 ? [responseArray] : nil)
					UIApplication.shared.isNetworkActivityIndicatorVisible = false
				}
				catch let respError {
					print(respError)
				}
			})
			task.resume()
		}
		catch let postError {
			print(postError)
		}
	}
	

}

